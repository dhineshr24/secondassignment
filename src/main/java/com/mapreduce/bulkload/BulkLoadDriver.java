package com.mapreduce.bulkload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;


public class BulkLoadDriver extends Configured implements Tool {

    private Path INPUT;
    private Path OUTPUT;
    private TableName TABLE_NAME;
    private byte[] CF_NAME;

    /*public static void main(String args[]) throws Exception {

        int status = ToolRunner.run(new BulkLoadDriver(),args);
        System.exit(status);

    }*/

    public BulkLoadDriver(Path input,Path output,TableName tableName,byte[] cfName){

        this.TABLE_NAME = tableName;
        this.CF_NAME = cfName;
        this.INPUT = input;
        this.OUTPUT = output;

    }

    @Override
    public int run(String[] args) throws Exception {

        Configuration config = HBaseConfiguration.create(getConf());
        setConf(config);

        Job job = getJob(config);
        loadConfiguration(config, job);

        boolean success = job.waitForCompletion(true);

        doBulkLoad();

        return success?0:1;

    }

    @SuppressWarnings("deprecation")
    private void doBulkLoad() {

        LoadIncrementalHFiles loader = new LoadIncrementalHFiles(getConf());
        try (Connection connection = ConnectionFactory.createConnection(getConf());

            Admin admin = connection.getAdmin()) {
            Table table = connection.getTable(TABLE_NAME);
            RegionLocator regionLocator = connection.getRegionLocator(TABLE_NAME);
            loader.doBulkLoad(OUTPUT, admin, table, regionLocator);

        }
        catch (TableNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void loadConfiguration(Configuration config, Job job) {

        try (Connection connection = ConnectionFactory.createConnection(config)) {

            Table table = connection.getTable(TABLE_NAME);
            Admin admin = connection.getAdmin();
            createTable(admin); // creates table if not already present in HBase
            RegionLocator regionLocator = connection.getRegionLocator(TABLE_NAME); //bulk load into hbase
            HFileOutputFormat2.configureIncrementalLoad(job, table, regionLocator);

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Job getJob(Configuration config)  {

        Job job = null;
        try {
            job = Job.getInstance(config,"Bulk Load");
            job.setJarByClass(BulkLoadMapper.class);
            job.setMapperClass(BulkLoadMapper.class);
            job.setMapOutputKeyClass(ImmutableBytesWritable.class);
            job.setMapOutputValueClass(Put.class);
            FileOutputFormat.setOutputPath(job, OUTPUT);
            FileInputFormat.addInputPath(job, INPUT);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return job;
        
    }

    private void createTable(final Admin admin) throws IOException {
        if(!admin.tableExists(TABLE_NAME)) {
            TableDescriptor desc = TableDescriptorBuilder.newBuilder(TABLE_NAME)
                    .setColumnFamily(ColumnFamilyDescriptorBuilder.of(CF_NAME))
                    .build();
            admin.createTable(desc);
        }
    }


}
