package com.mapreduce.bulkload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class RetrieveData {

    static final String TABLE_NAME = "person";

    private static final byte[] PERSON_NAME = Bytes.toBytes("name");
    private static final byte[] PERSON_AGE = Bytes.toBytes("age");
    private static final byte[] PERSON_COMPANY = Bytes.toBytes("company");
    private static final byte[] PERSON_BUILDING_CODE = Bytes.toBytes("building_code");
    private static final byte[] PERSON_PHONE_NUMBER = Bytes.toBytes("phone_number");
    private static final byte[] PERSON_ADDRESS = Bytes.toBytes("address");

    public static void main(String[] args) throws IOException, Exception{


        Configuration config = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(config);
        Admin admin = connection.getAdmin();

        Table table = connection.getTable(TableName.valueOf(TABLE_NAME));

        Get g = new Get(Bytes.toBytes("row0"));

        Result result = table.get(g);

        byte [] name = result.getValue(Bytes.toBytes("information"),PERSON_NAME);
        byte [] age = result.getValue(Bytes.toBytes("information"),PERSON_AGE);
        byte [] company = result.getValue(Bytes.toBytes("information"),PERSON_COMPANY);
        byte [] building_code = result.getValue(Bytes.toBytes("information"),PERSON_BUILDING_CODE);
        byte [] phone_number = result.getValue(Bytes.toBytes("information"),PERSON_PHONE_NUMBER);
        byte [] address = result.getValue(Bytes.toBytes("information"),PERSON_ADDRESS);

        // Printing the values
        String person_name = Bytes.toString(name);
        String person_age = Bytes.toString(age);
        String person_company = Bytes.toString(company);
        String person_building_code = Bytes.toString(building_code);
        String person_phone_number = Bytes.toString(phone_number);
        String person_address = Bytes.toString(address);

        System.out.println(person_name.getClass().getSimpleName() + " " + person_address+ " " +person_age+ " " +person_company
                + " " +person_building_code+ " " +person_phone_number);


    }
}
