package com.mapreduce.bulkload;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

//TableMapper <Key,Value> (key = rowID,value = columns)
public class BulkLoadMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {

    private static final byte[] CF_NAME = Bytes.toBytes("information");

    private final byte[][] QUAL_BYTES = {"name".getBytes(), "age".getBytes(),"company".getBytes(),
            "building_code".getBytes(),"phone_number".getBytes(),
              "address".getBytes()};

    @Override
    protected void map(LongWritable key, Text value,
                       Mapper<LongWritable, Text, ImmutableBytesWritable, Put>.Context context) throws IOException, InterruptedException {

        if(value.getLength()==0 || key.get()==0){
            return;
        }

        String data[] = value.toString().split(",",6);
        byte[] rowKey = Bytes.toBytes(String.valueOf(key.get()));

        Put put = new Put(rowKey);

        put.addColumn(CF_NAME,QUAL_BYTES[0], Bytes.toBytes(data[0]));
        put.addColumn(CF_NAME,QUAL_BYTES[1], Bytes.toBytes(data[1]));
        put.addColumn(CF_NAME,QUAL_BYTES[2], Bytes.toBytes(data[2]));
        put.addColumn(CF_NAME,QUAL_BYTES[3], Bytes.toBytes(data[3]));
        put.addColumn(CF_NAME,QUAL_BYTES[4], Bytes.toBytes(data[4]));
        put.addColumn(CF_NAME,QUAL_BYTES[5], Bytes.toBytes(data[5]));

        context.write(new ImmutableBytesWritable(rowKey), put);
    }
}
